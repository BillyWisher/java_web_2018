package DAO;

import javax.naming.NamingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import beans.Book;
import DB.DBConnector;


/**
 * The type Db book dao.
 */
public class DBBookDAO implements IBookDAO {
    private static final Logger logger = Logger.getLogger(DBBookDAO.class.getName());
    private static final String GET_BOOKS_QUERY = "SELECT * FROM books;";

    @Override
    public List<Book> getBooks() {
        List<Book> books = new ArrayList<>();
        try (Connection connection = new DBConnector().getConnection();
            PreparedStatement statement = connection.prepareStatement(DBBookDAO.GET_BOOKS_QUERY);
            ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                books.add(new Book(resultSet));
                for(Book bk : books){
                    System.out.println(bk.getId());
                }
            }
            return books;
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "SQL Exception in Database DAO implementation while getting book list", e);
        } catch (NamingException e) {
            logger.log(Level.SEVERE, "Naming Exception in Database DAO implementation while getting book list", e);
        }
        return books;
    }
}
