package DAO;
import beans.Book;
import java.util.List;

public interface IBookDAO {

    List<Book> getBooks();
}
