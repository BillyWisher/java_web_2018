package DAO;

/**
 * The type Book dao factory.
 */
public class BookDAOFactory {


    /**
     * Get class from factory book dao.
     *
     * @return the book dao
     */
    public IBookDAO getClassFromFactory() {
        return new DBBookDAO();
    }
}
