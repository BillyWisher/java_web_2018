package DB;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * The type Db connector.
 */
public class DBConnector {

    private static final String BASE_PATH = "jdbc/books_list";

    /**
     * Gets connection.
     *
     * @return the connection
     * @throws NamingException the naming exception
     * @throws SQLException    the sql exception
     */
    public Connection getConnection() throws NamingException, SQLException {
        final Context context = (Context) new InitialContext().lookup("java:/comp/env");
        final DataSource dataSource = (DataSource) context.lookup(DBConnector.BASE_PATH);
        return dataSource.getConnection();
    }

}

