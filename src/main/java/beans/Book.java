package beans;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The type Book.
 */
public class Book {
    private Integer id;
    private String title;
    private String author;

    private static final Logger logger = Logger.getLogger(Book.class.getName());
    private static final int ID_QUERY_PLACE = 1;
    private static final int TITLE_QUERY_PLACE = 2;
    private static final int AUTHOR_QUERY_PLACE = 3;

    /**
     * Instantiates a new Book.
     *
     * @param id     the id
     * @param title  the title
     * @param author the author
     */
    public Book(final Integer id, final String title, final String author) {
        this.id = id;
        this.title = title;
        this.author = author;
    }

    /**
     * Instantiates a new Book.
     *
     * @param resultSet the result set
     */
    public Book (final ResultSet resultSet){
        try {
            this.id = resultSet.getInt(ID_QUERY_PLACE);
            this.title = resultSet.getString(TITLE_QUERY_PLACE);
            this.author = resultSet.getString(AUTHOR_QUERY_PLACE);
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "SQL Exception in Database DAO implementation while getting book list", e);
        }
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Gets title.
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Gets author.
     *
     * @return the author
     */
    public String getAuthor() {
        return author;
    }

}
