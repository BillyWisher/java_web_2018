package controllers;

import javax.servlet.ServletConfig;
import javax.servlet.UnavailableException;
import javax.servlet.http.HttpServlet;

/**
 * The type Abstract controller.
 */
public abstract class AbstractController extends HttpServlet {

    protected boolean available;
    //tried atomic but got messed up



    protected boolean isAvailable(final ServletConfig config) {
        if (!Boolean.parseBoolean(config.getInitParameter("isAvailable"))) {
            return false;
        }
        return true;
    }
    protected void makeUnavailable(final ServletConfig config) throws UnavailableException {
        if (!this.available) {
            StringBuilder msgBuilder = new StringBuilder().append(config.getServletName()).append(" is unavailable");
            int unavailableTime = Integer.parseInt(config.getInitParameter("unavailableTime"));
            if (unavailableTime > 0) {
                this.available = true;
            }
            throw new UnavailableException(msgBuilder.toString(), unavailableTime);
        }
    }
}
