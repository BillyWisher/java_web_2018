package controllers;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import DAO.BookDAOFactory;
import beans.Book;


/**
 * The type Main controller.
 */
public class MainController extends AbstractController {
    private static final String ATTR_BOOKS = "books";
    private static final String PATH_MAIN = "/main.jsp";

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        List<Book> books = new BookDAOFactory().getClassFromFactory().getBooks();
        req.setAttribute(MainController.ATTR_BOOKS, books);
        req.getRequestDispatcher(PATH_MAIN).forward(req, resp);
        if (!super.available && !super.isAvailable(this.getServletConfig())) {
            super.makeUnavailable(this.getServletConfig());
        }
    }
}
