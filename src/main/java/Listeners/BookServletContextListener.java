package Listeners;


import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.logging.Logger;
import javax.servlet.ServletContext;


/**
 * The type Book servlet context listener.
 */
public class BookServletContextListener implements ServletContextListener {

    private static final Logger logger = Logger.getLogger(BookServletContextListener.class.getName());

    @Override
    public void contextDestroyed(final ServletContextEvent arg0) {
        this.log(arg0.getServletContext(), "Application started");
    }

    @Override
    public void contextInitialized(final ServletContextEvent arg0) {
        this.log(arg0.getServletContext(), "Application stopped");
    }

    /**
     * Log.
     *
     * @param msg the msg
     */
    protected void log(final ServletContext context, final String msg) {
        String serverInfo = context.getServerInfo();
        String applicationName = context.getServletContextName();
        StringBuilder logBuilder = new StringBuilder().append(serverInfo).append(": Application ")
                .append(applicationName).append(" : ").append(msg);
        logger.info(logBuilder.toString());
    }
}
