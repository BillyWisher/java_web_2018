<%--
  Created by IntelliJ IDEA.
  User: TOK
  Date: 14.06.2018
  Time: 9:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Main</title>
    <style>

        article {
            text-align: center;
            margin-top: 30px;
        }
        header {
            color: black;
            font-size: 14pt;
            text-transform: uppercase;
        }

        p {
            margin-top: 10px;
        }

        hr {
            border: none;
            background: antiquewhite;
            height: 1px;
            margin: 10px 20%;
            text-align: center;
        }
    </style>
</head>
<body>
<div>
    <article>
        <header>
            <b>Book list </b>
        </header>
        <c:forEach items="${books}" var="book" varStatus="container">
            <hr>
            <p>${book.id}; ${book.title}; ${book.author};</p>
        </c:forEach>
        <hr>
    </article>
</div>
</body>
</html>
